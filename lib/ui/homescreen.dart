import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './Catagories.dart';
import 'package:carousel_slider/carousel_slider.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}
class HomeScreenState extends State<StatefulWidget> {
  int _index = 0;
  final List<String> imageList = [
    "https://salt.tikicdn.com/ts/brickv2og/e4/a7/2d/e0ade41317911a06b355e49d7dded552.png",
    "https://salt.tikicdn.com/cache/w1240/ts/brickv2og/b7/a7/80/33399afbaaeb75581fdbcab1366da464.png.webp",
    "https://salt.tikicdn.com/cache/w1240/ts/brickv2og/76/c4/5b/ab5344f0086b1f81c01ec6584088c4c7.png.webp",
    'https://salt.tikicdn.com/cache/w940/ts/brickv2og/6b/5c/6b/ac27c8dff058c77b2bb66d226805c05b.png.webp'
  ];
  static const TextStyle optionStyle = TextStyle(
      fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Trang Chủ',
      style: optionStyle,
    ),
    Text(
      'Index 1: Danh Mục',
      style: optionStyle,
    ),
    Text(
      'Index 2: Lướt',
      style: optionStyle,
    ),
    Text(
      'Index 3: Chat',
      style: optionStyle,
    ),
    Text(
      'Index 4: Cá Nhân',
      style: optionStyle,
    ),
  ];

  void _onClick(int index) {
    setState(() {
      _index = index;
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const Catagories()),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        appBar: AppBar(
            title: Align(
              child: Text("TIKI"),
              alignment: Alignment.center,
            ),
            leading: Align(
              child: Text("FREESHIP"),
              alignment: Alignment.centerLeft,
            ),
            leadingWidth: 100.0,
            backgroundColor: Colors.lightGreen,
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.add_alert_sharp),
                onPressed: () =>
                {
                  print("Click on notificate button")
                },
              ),
              IconButton(
                  icon: Icon(Icons.shopping_cart),
                  onPressed: () =>
                  {
                    print("Click on shopping cart button")
                  }
              ),
            ]

        ),
        body: Home(),
        backgroundColor: Colors.lightGreen,
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined),
              label: 'Trang Chủ',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.apps_sharp),
              label: 'Danh Mục',

            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.local_fire_department_outlined),
              label: 'Lướt',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.message_outlined),

              label: 'Chat',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person_outlined),
              label: 'Cá Nhân',
            ),
          ],
          currentIndex: _index,
          unselectedItemColor: Colors.grey,
          selectedItemColor: Colors.blueAccent,

          onTap: _onClick,
        )
    );
  }

  Widget Home() {
    return Container(
      child: Column(
        children: [
          SearchField(),
          Tab(),
          SaleSlide(),
          FlashSale(),
          Bar()
        ],
      ),
    );
  }

  Widget SearchField() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.grey.withOpacity(0.5),
          width: 1.0,
        ),
      ),
      margin: EdgeInsets.all(12),
      child: Row(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(left: 8),
          child: Icon(
          Icons.search,
          color: Colors.grey,
            size: 20,
          ),
        ),
        Expanded(
        child: TextField(
        keyboardType: TextInputType.text,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Nhấn vào các biểu tượng trên bottom bar để chuyển trang",
          ),
        ),
        )
      ],
      ),
    );
}



Widget Tab() {
    return Container(
        padding: EdgeInsets.all(5.0),
        child: Row(

            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(

                child: Text('Thịt, Rau Củ'),
                flex: 3

              ),
              Expanded(
                child: Text('Bách Hóa'),
                flex: 2
              ),
              Expanded(
                child:  Text('Nhà Cửa'),
                  flex: 2
              ),
              Expanded(
                child: Text('Điện Tử'),
                  flex: 2
              ),
              Expanded(
                child: Icon(Icons.window_outlined,),
                  flex: 0

              ),
            ]
        )
    );
  }
  Widget SaleSlide() {
    return Container(
      child: CarouselSlider(
        options: CarouselOptions(
            enlargeCenterPage: true,
            autoPlay: true,
            aspectRatio: 16 / 9,
            autoPlayCurve: Curves.fastOutSlowIn,
            enableInfiniteScroll: true,
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            viewportFraction: 0.8
        ),
        items: imageList.map((e) => ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Stack(
            fit: StackFit.expand,

            children: <Widget>[
              Image.network(e,
                fit: BoxFit.cover,

                )
            ],
          ) ,
        )).toList(),

      ),
    );

  }
  Widget FlashSale () {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget> [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            "Giá Sốc Hôm Nay",
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w600,
              letterSpacing: 1.0,
              color: Colors.white
            ),
          ),
        ),
        Container(
            child: Row(

                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [

                  Expanded(
                    child: Image.asset('lib/image/ss11.png'),

                  ),
                  Expanded(
                    child: Image.asset('lib/image/tiki2.png'),
                  ),
                  Expanded(
                    child: Image.asset('lib/image/ss11.png'),

                  ),
                ]
            )
        )
      ],

    );

  }
  Widget Bar () {
    return Container(
        margin: const EdgeInsets.only(top: 10.0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40),
                topRight: Radius.circular(40)
            )
        ),
        child: Row(
          children: [

            buildRow ("4","Bí kíp săn sale     "),
            buildRow ("5","Săn thưởng mỗi ngày     "),
            buildRow ("6",'Săn đồ Si mê'),

          ],
        )
    );

  }
  Padding buildRow (String img, String title){
    return Padding(
      padding: const EdgeInsets.all(1.0),
      child: Column(
      children: [
        Container(
          height: 95,
          width: 40,
          child: Image.asset('lib/image/tiki$img.png',
          height: 85 ,),
        ),
        Text(title,)
      ],
      )
    );
  }
}

