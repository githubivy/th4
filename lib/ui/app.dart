import 'package:flutter/material.dart';
import './homescreen.dart';

class TiKiApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomeScreen(),
    );
  }

}
