import 'package:flutter/material.dart';
import 'dart:async';
import './homescreen.dart';

class Catagories extends StatefulWidget {

  const Catagories({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CatagoriesScreenState();
  }

}
class CatagoriesScreenState extends State<StatefulWidget> {
  int _index = 0;
  void _onClick(int index) {
    setState(() {
      _index = index;
      Navigator.pop(context);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back_outlined),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
            backgroundColor: Colors.lightGreen,
            actions: <Widget>[
            IconButton(
              icon: Icon(Icons.add_alert_sharp),
              onPressed: () =>
              {

              }
            ),
            IconButton(
                icon: Icon(Icons.shopping_cart),
                onPressed: () =>
                {

                }
            ),
          ]

      ),
      body: Body(),
      backgroundColor: Colors.lightGreen,
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home_outlined),
            label: 'Trang Chủ',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.apps_sharp),
            label: 'Danh Mục',

          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.local_fire_department_outlined),
            label: 'Lướt',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.message_outlined),

            label: 'Chat',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_outlined),
            label: 'Cá Nhân',
          ),
        ],
        currentIndex: _index,
         unselectedItemColor: Colors.grey,
        selectedItemColor: Colors.blueAccent,

        onTap: _onClick,
      )
    );
  }
  Widget Body() {
    return Container(
      child: Column(
        children: [
          SearchField()

        ],
      ),
    );
  }
  Widget SearchField() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.grey.withOpacity(0.5),
          width: 1.0,
        ),
      ),
      margin: EdgeInsets.all(12),
      child: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 8),
            child: Icon(
              Icons.search,
              color: Colors.grey,
              size: 20,
            ),
          ),
          Expanded(
            child: TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "Nhấn vào các biểu tượng trên bottom bar để chuyển trang",
              ),
            ),
          )
        ],
      ),
    );
  }

}